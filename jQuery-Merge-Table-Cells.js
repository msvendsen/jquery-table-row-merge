//loop over each table row
jQuery('table tbody tr').each(function(){
  
  var row = jQuery(this);
  var cells = row.find('td');
  var newCells = [];

  //loop over each cell in the row
  for(var i = 0; i < cells.length; i++){
    //get the individual cell value
    var cell = cells[i];
    //ceate a new cell for the first element
    if(i == 0){
      newCells.push(createNewCell(jQuery(cell).html()));
    } else {
      //cell is not the original check the values
      if(jQuery(cell).html() == jQuery(cells[i - 1]).html()){
        //non original cell -- expand colspan
        addToColspan(newCells[newCells.length - 1]);
      } else {
        //original cell create new cell with value
        newCells.push(createNewCell(jQuery(cell).html()));
      }
    }
    jQuery(cell).remove();
  }

  //add in the new cells
  for(var j = 0; j < newCells.length; j++){
    row.append(newCells[j]);
  }
  

});

function createNewCell(data){
  var newCell = jQuery('<td>', {
    colspan: 1
  });
  newCell.html(data);
  return newCell;
}

function addToColspan(element){
  var currentSpan = Number(jQuery(element).attr('colspan'));
  jQuery(element).attr('colspan', currentSpan + 1);
}